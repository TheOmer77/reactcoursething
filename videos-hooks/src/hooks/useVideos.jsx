import { useState, useEffect } from 'react';
import youtube from '../api/youtube';

const useVideos = (searchTerm) => {
  const [videos, setVideos] = useState([]);

  useEffect(() => search(searchTerm), [searchTerm]);

  const search = async (term) => {
    try {
      const response = await youtube.get('/search', {
        params: { q: term },
      });
      setVideos(response.data.items);
    } catch (err) {
      console.log(err);
    }
  };

  return [videos, search];
};

export default useVideos;
