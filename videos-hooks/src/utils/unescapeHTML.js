const unescapeHTML = (html) => {
  let escapeEl = document.createElement('textarea');
  escapeEl.innerHTML = html;
  return escapeEl.textContent;
};

export default unescapeHTML;
