import axios from 'axios';

const BASE_URL = 'https://www.googleapis.com/youtube/v3';
const KEY = 'AIzaSyBeXUey7CYZgSGXMLst4PlvZ8IGmftaIKE';
const MAX_RESULTS = 8;

export default axios.create({
  baseURL: BASE_URL,
  params: {
    key: KEY,
    part: 'snippet',
    type: 'video',
    maxResults: MAX_RESULTS,
  },
});
