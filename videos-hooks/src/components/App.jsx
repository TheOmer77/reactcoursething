import React, { useState, useEffect } from 'react';
import useVideos from '../hooks/useVideos';

// Components
import Header from './Header';
import VideoList from './VideoList';
import VideoView from './VideoView';

import youtube from '../api/youtube';

const App = () => {
  const [currentVideo, setCurrentVideo] = useState(null);
  const [videos, search] = useVideos('');

  useEffect(() => {
    setCurrentVideo(null);
  }, [videos]);

  const handleVideoSelect = async (video) => {
    const { videoId } = video.id;
    const response = await youtube.get('/videos', { params: { id: videoId } });
    setCurrentVideo(response.data.items[0]);
  };

  return (
    <div>
      <Header onSearchSubmit={search} />
      <div className='ui container'>
        <div className='ui stackable grid'>
          <div className='ui row'>
            {currentVideo && (
              <div className='eleven wide column'>
                <VideoView video={currentVideo} />
              </div>
            )}
            <div className={`${currentVideo ? 'five' : 'sixteen'} wide column`}>
              {videos.length > 0 ? (
                <VideoList videos={videos} onVideoSelect={handleVideoSelect} />
              ) : (
                <span>Loading...</span>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default App;
