import React, { useState } from 'react';
import Searchbar from './Searchbar';

import './Header.css';

const Header = ({ onSearchSubmit }) => {
  const [term, setTerm] = useState('');

  const handleFormSubmit = (e) => {
    e.preventDefault();
    onSearchSubmit(term);
  };

  return (
    <div className='ui red inverted basic segment'>
      <div className='ui container _header'>
        <form className='ui form' onSubmit={handleFormSubmit}>
          <div className='two fields'>
            <div className='field logo'>
              <i className='big video icon'></i>
              <h1>FakeTube</h1>
            </div>
            <div className='field'>
              <Searchbar
                value={term}
                onChange={(e) => setTerm(e.target.value)}
              />
            </div>
          </div>
        </form>
      </div>
    </div>
  );
};

export default Header;
