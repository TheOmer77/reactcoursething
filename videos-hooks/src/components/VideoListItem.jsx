import React from 'react';
import unescapeHTML from '../utils/unescapeHTML';

import './VideoListItem.css';

const VideoListItem = ({ video, onClick }) => {
  const { title, thumbnails, channelTitle, publishedAt } = video.snippet;
  return (
    <div className='link item video-list-item' onClick={() => onClick(video)}>
      <img className='ui small image' src={thumbnails.medium.url} alt={title} />
      <div className='content'>
        <h4 className='header'>{unescapeHTML(title)}</h4>
        <div className='description'>
          {`${unescapeHTML(channelTitle)}, ${new Date(
            publishedAt
          ).toLocaleDateString()}`}
        </div>
      </div>
    </div>
  );
};

export default VideoListItem;
