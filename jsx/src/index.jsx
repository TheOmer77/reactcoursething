// Import the React & ReactDOM libraries
import React from 'react';
import ReactDOM from 'react-dom';

// Create a react component

const getTime = () => {
  return new Date().toLocaleTimeString();
};

const App = () => {
  return (
    <div>
      <div>Current Time:</div>
      <h3>{getTime()}</h3>
    </div>
  );
};

// Take the react component and show it on the screen
ReactDOM.render(<App />, document.getElementById('root'));
