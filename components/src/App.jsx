// Components
import ApprovalCard from './components/ApprovalCard';
import Comment from './components/Comment';

const App = () => {
  return (
    <div className='ui container comments'>
      <ApprovalCard>
        <div>
          <h4>Warning!</h4>Are you sure you want to approve this?
        </div>
      </ApprovalCard>
      <ApprovalCard>
        <Comment
          author='Johnny Diaz'
          time='Today, 15:35'
          img='https://randomuser.me/api/portraits/men/71.jpg'
          content='Yayaya'
        />
      </ApprovalCard>
      <ApprovalCard>
        <Comment
          author='Tonya Jensen'
          time='Today, 14:00'
          img='https://randomuser.me/api/portraits/women/59.jpg'
          content='Hahaha'
        />
      </ApprovalCard>
      <ApprovalCard>
        <Comment
          author='Neil Murphy'
          time='Yesterday, 17:20'
          img='https://randomuser.me/api/portraits/men/59.jpg'
          content='Bababa'
        />
      </ApprovalCard>
    </div>
  );
};

export default App;
