import React from 'react';

const App = () => {
  return (
    <div>
      <Message
        header='Changes in terms of service'
        body='We are changing our terms of service. By accepting the new terms, you give us permission to track you and everything you do, and sell information about you to advertisers.'
      />
    </div>
  );
};

const Message = ({ header, body }) => (
  <div>
    <div className='ui message'>
      <div className='header'>{header}</div>
      <p>{body}</p>
    </div>
  </div>
);
export default App;
