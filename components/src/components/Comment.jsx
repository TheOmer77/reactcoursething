import React from 'react';

const Comment = ({ author, time, img, content }) => (
  <div className='comment'>
    <a href='#' className='avatar'>
      <img src={img} alt='avatar' />
    </a>
    <div className='content'>
      <a href='#' className='author'>
        {author}
      </a>
      <div className='metadata'>
        <span className='date'>{time}</span>
      </div>
      <div className='text'>{content}</div>
    </div>
  </div>
);

export default Comment;
