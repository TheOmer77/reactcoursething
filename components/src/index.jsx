import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import PropsExercise from './PropsExercise';
import ChildrenExercise from './ChildrenExercise';

ReactDOM.render(
  <React.StrictMode>
    <ChildrenExercise />
  </React.StrictMode>,
  document.getElementById('root')
);
