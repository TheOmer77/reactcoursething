import React from 'react';

const ChildrenExercise = () => {
  return (
    <div>
      <Segment>
        <div className='ui icon header'>
          <i className='pdf file outline icon'></i>
          No documents are listed for this customer.
          <div className='ui primary button'>Add Document</div>
        </div>
      </Segment>
      <Segment>
        <h4 className='ui header'>For your information</h4>
        <p>
          Lorem ipsum, dolor sit amet consectetur adipisicing elit. Maxime,
          doloribus voluptates! Itaque maxime ab numquam, minus dolor asperiores
          eveniet inventore aut totam! Aliquam ut laborum totam rem,
          reprehenderit tempora atque deleniti facilis ab doloremque placeat.
          Ipsa voluptatum eligendi excepturi asperiores nobis quis. Alias harum
          ducimus ab nobis molestias commodi aliquam laboriosam a ipsum id
          repudiandae, soluta quaerat nam eaque deserunt distinctio laudantium
          architecto adipisci exercitationem autem culpa minima itaque quia!
          Magnam commodi exercitationem odio et nihil, porro veritatis molestias
          quasi saepe doloribus voluptatem ipsa iusto illo recusandae quaerat
          optio, mollitia in reprehenderit aliquid. Eum ullam atque sit dolor
          reprehenderit voluptatum!
        </p>
      </Segment>
    </div>
  );
};

const Segment = ({ children }) => (
  <div className='ui placeholder segment'>{children}</div>
);

export default ChildrenExercise;
