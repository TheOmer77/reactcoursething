/**
 * Map an array to an object.
 * @param {object[]} array The array to map.
 * @param {string} key The key of each object in the array to use as its key in the returned object.
 * @returns {object} An object with the elements of the original array.
 */
export const mapKeys = (array, key) => {
  const obj = {};
  array.forEach((element) => (obj[element[key]] = element));
  return obj;
};

/**
 * Create an object composed of the object properties predicate returns truthy for.
 * Copied from https://github.com/you-dont-need/You-Dont-Need-Lodash-Underscore#_pick
 * @param {object} object
 * @param {[]} keys
 */
export const pick = (object, keys) => {
  return keys.reduce((obj, key) => {
    if (object && object.hasOwnProperty(key)) {
      obj[key] = object[key];
    }
    return obj;
  }, {});
};
