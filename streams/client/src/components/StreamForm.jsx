import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';

class StreamForm extends Component {
  renderInput = ({ input, label, id, meta }) => {
    const { error, touched } = meta;
    return (
      <div className={`field${touched && error ? ' error' : ''}`}>
        <label htmlFor={id}>{label}</label>
        <input {...input} id={id} autoComplete='off' />
        {touched && error && <label>{error}</label>}
      </div>
    );
  };

  onSubmit = (formValues) => {
    this.props.onSubmit(formValues);
  };

  render() {
    return (
      <div>
        <form
          className='ui form error'
          onSubmit={this.props.handleSubmit(this.onSubmit)}
        >
          <Field
            name='title'
            id='input_title'
            label='Title'
            component={this.renderInput}
          />
          <Field
            name='description'
            id='input_description'
            label='Description'
            component={this.renderInput}
          />
          <button className='ui violet button'>Submit</button>
        </form>
      </div>
    );
  }
}

const validate = (formValues) => {
  const errors = {};

  if (!formValues.title) errors.title = 'You must enter a title.';
  if (!formValues.description)
    errors.description = 'You must enter a description.';

  return errors;
};

export default reduxForm({ form: 'streamForm', validate })(StreamForm);
