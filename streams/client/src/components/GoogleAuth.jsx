import React, { Component } from 'react';
import { connect } from 'react-redux';
import { signIn, signOut } from '../actions';
import { clientId } from '../clientId.json';

const { gapi } = window;

class GoogleAuth extends Component {
  componentDidMount() {
    gapi.load('client:auth2', () => {
      gapi.client
        .init({ clientId, scope: 'email' })
        .then(() => {
          this.auth = gapi.auth2.getAuthInstance();
          // this.setState({ isSignedIn: this.auth.isSignedIn.get() });
          this.onAuthChange(this.auth.isSignedIn.get());
          this.auth.isSignedIn.listen(this.onAuthChange);
        })
        .catch((err) => console.error(err));
    });
  }

  onAuthChange = (isSignedIn) =>
    isSignedIn
      ? this.props.signIn(this.auth.currentUser.get().getId())
      : this.props.signOut();

  onSignInClick = () => this.auth.signIn();
  onSignOutClick = () => this.auth.signOut();

  renderAuthBtn() {
    return this.props.isSignedIn === null ? null : this.props.isSignedIn ? (
      <button className='ui inverted button' onClick={this.onSignOutClick}>
        Sign out
      </button>
    ) : (
      <button
        className='ui inverted google button'
        onClick={this.onSignInClick}
      >
        <i className='google icon'></i>
        Sign in with Google
      </button>
    );
  }

  render() {
    return <div className='item'>{this.renderAuthBtn()}</div>;
  }
}

const mapStateToProps = (state) => ({
  isSignedIn: state.auth.isSignedIn,
});

export default connect(mapStateToProps, { signIn, signOut })(GoogleAuth);
