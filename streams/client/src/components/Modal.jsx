import React from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';

const Modal = ({ title, content, actions, onDismiss }) => {
  return ReactDOM.createPortal(
    <div className='ui dimmer modals visible active' onClick={onDismiss}>
      <div
        className='ui standard small modal visible active'
        onClick={(e) => e.stopPropagation()}
      >
        <div className='header'>{title}</div>
        <div className='content'>{content}</div>
        <div className='actions'>
          {actions.map((action) =>
            action.isLink ? (
              <Link
                className={`ui button ${action.className || ''}`}
                to={action.to}
                key={action.text}
              >
                {action.text}
              </Link>
            ) : (
              <button
                className={`ui button ${action.className || ''}`}
                onClick={action.onClick}
                key={action.text}
              >
                {action.text}
              </button>
            )
          )}
        </div>
      </div>
    </div>,
    document.getElementById('modal')
  );
};

export default Modal;
