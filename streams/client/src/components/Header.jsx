import React from 'react';
import { Link } from 'react-router-dom';
import GoogleAuth from './GoogleAuth';

const Header = () => {
  return (
    <div className='ui inverted violet basic segment _header'>
      <div className='ui container'>
        <div className='ui inverted secondary menu'>
          <div className='ui yellow inverted header item'>STREAMEE</div>
          <Link to='/' className='active item'>
            All Streams
          </Link>
          <div className='right menu'>
            <GoogleAuth />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Header;
