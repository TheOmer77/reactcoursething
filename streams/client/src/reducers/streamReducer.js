import {
  CREATE_STREAM,
  DELETE_STREAM,
  EDIT_STREAM,
  FETCH_STREAM,
  FETCH_STREAMS,
} from '../actions/types.json';
import { mapKeys } from '../utils/fakeLodash';

const INITIAL_STATE = {};

const streamReducer = (state = INITIAL_STATE, { type, payload }) => {
  switch (type) {
    case FETCH_STREAMS:
      return { ...state, ...mapKeys(payload, 'id') };
    case FETCH_STREAM:
      return { ...state, [payload.id]: payload };
    case CREATE_STREAM:
      return { ...state, [payload.id]: payload };
    case EDIT_STREAM:
      return { ...state, [payload.id]: payload };
    case DELETE_STREAM:
      const { [payload]: omit, ...newState } = state;
      return newState;
    default:
      return state;
  }
};

export default streamReducer;
