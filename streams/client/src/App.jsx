import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';

import history from './utils/history';

// CSS
import './App.css';
import './icons.css';

// Components
import Header from './components/Header';
import StreamCreate from './screens/StreamCreate';
import StreamDelete from './screens/StreamDelete';
import StreamEdit from './screens/StreamEdit';
import StreamList from './screens/StreamList';
import StreamShow from './screens/StreamShow';

const App = () => {
  return (
    <div>
      <Router history={history}>
        <Header />
        <div className='ui container'>
          <Switch>
            <Route path='/' exact component={StreamList} />
            <Route path='/streams/new' exact component={StreamCreate} />
            <Route path='/streams/edit/:id' exact component={StreamEdit} />
            <Route path='/streams/delete/:id' exact component={StreamDelete} />
            <Route path='/streams/:id' exact component={StreamShow} />
          </Switch>
        </div>
      </Router>
    </div>
  );
};

export default App;
