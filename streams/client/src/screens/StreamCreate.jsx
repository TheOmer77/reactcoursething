import React, { Component } from 'react';
import { connect } from 'react-redux';

import { createStream } from '../actions/';

// Components
import StreamForm from '../components/StreamForm';

class StreamCreate extends Component {
  onSubmit = (formValues) => this.props.createStream(formValues);

  render() {
    return (
      <div>
        <h1>Create Stream</h1>
        <StreamForm onSubmit={this.onSubmit} />
      </div>
    );
  }
}

export default connect(null, { createStream })(StreamCreate);
