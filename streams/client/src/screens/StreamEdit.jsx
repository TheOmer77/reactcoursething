import React, { Component } from 'react';
import { connect } from 'react-redux';

import { fetchStream, editStream } from '../actions';
import { pick } from '../utils/fakeLodash';

// Components
import StreamForm from '../components/StreamForm';

class StreamEdit extends Component {
  componentDidMount() {
    this.props.fetchStream(this.props.match.params.id);
  }

  onSubmit = (formValues) =>
    this.props.editStream(this.props.match.params.id, formValues);

  render() {
    return this.props.stream ? (
      <div>
        <h1>Edit Stream</h1>
        <StreamForm
          onSubmit={this.onSubmit}
          initialValues={pick(this.props.stream, ['title', 'description'])}
        />
      </div>
    ) : (
      <div>Loading...</div>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  stream: state.streams[ownProps.match.params.id],
});

export default connect(mapStateToProps, { fetchStream, editStream })(
  StreamEdit
);
