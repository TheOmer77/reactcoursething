import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchStream, deleteStream } from '../actions';
import history from '../utils/history';

// Components
import Modal from '../components/Modal';

class StreamDelete extends Component {
  streamId = this.props.match.params.id;

  modalActions = [
    {
      text: 'Yes',
      className: 'negative',
      onClick: () => this.props.deleteStream(this.streamId),
    },
    {
      text: 'No',
      isLink: true,
      to: '/',
    },
  ];

  componentDidMount() {
    this.props.fetchStream(this.streamId);
  }

  render() {
    return (
      <Modal
        title='Delete Stream'
        content={`Are you sure you want to delete the stream${
          this.props.stream ? ` "${this.props.stream.title}"` : ''
        }?`}
        actions={this.modalActions}
        onDismiss={() => history.push('/')}
      />
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  stream: state.streams[ownProps.match.params.id],
});

export default connect(mapStateToProps, { fetchStream, deleteStream })(
  StreamDelete
);
