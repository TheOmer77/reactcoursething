import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { fetchStreams } from '../actions';

// CSS
import './StreamList.css';

class StreamList extends Component {
  componentDidMount() {
    this.props.fetchStreams();
  }

  streamList(currentUserStreams) {
    const userStreams = this.props.streams.filter(
      (stream) => stream.userId === this.props.auth.userId
    );

    const mainContent = (stream) => (
      <div className='main-content'>
        <i className='big camera icon' />
        <div className='content'>
          <div className='ui header'>{stream.title}</div>
          <div>{stream.description}</div>
        </div>
      </div>
    );

    return userStreams.length > 0 || !currentUserStreams ? (
      <div className='ui grid'>
        <div className='row stream-item-row'>
          <div
            className={`${
              currentUserStreams ? 'thirteen' : 'sixteen'
            } wide column`}
          >
            <div className='ui secondary fluid vertical menu'>
              {this.props.streams.map(
                (stream) =>
                  (stream.userId === this.props.auth.userId) ===
                    currentUserStreams && (
                    <Link
                      to={`streams/${stream.id}`}
                      className='item stream-item'
                      key={stream.id}
                    >
                      {mainContent(stream)}
                    </Link>
                  )
              )}
            </div>
          </div>
          {currentUserStreams && (
            <div className='three wide column'>
              {this.props.streams.map(
                (stream) =>
                  (stream.userId === this.props.auth.userId) ===
                    currentUserStreams && this.streamBtns(stream.id)
              )}
            </div>
          )}
        </div>
      </div>
    ) : (
      <div>
        You do not have any streams. Create a stream by using the plus button
        above.
      </div>
    );
  }

  streamBtns(streamId) {
    return (
      <div key={streamId} className='right floated content'>
        <Link
          to={`/streams/edit/${streamId}`}
          className='ui violet basic button'
        >
          Edit
        </Link>
        <Link
          to={`/streams/delete/${streamId}`}
          className='ui negative basic button'
        >
          Delete
        </Link>
      </div>
    );
  }

  render() {
    return (
      <div>
        {this.props.auth.isSignedIn && (
          <div>
            <Link
              to='/streams/new'
              className='ui right floated violet icon button'
            >
              <i className='plus icon' />
            </Link>
            <h1>My Streams</h1>
            {this.streamList(true)}
          </div>
        )}
        <h1>Streams</h1>
        {this.streamList(false)}
      </div>
    );
  }
}

const mapStateToProps = ({ auth, streams }) => {
  return { streams: Object.values(streams), auth };
};

export default connect(mapStateToProps, { fetchStreams })(StreamList);
