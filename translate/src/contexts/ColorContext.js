import React, { useState } from 'react';

const Context = React.createContext('red');

export const ColorStore = ({ children }) => {
  const [color, setColor] = useState('red');

  return (
    <Context.Provider value={[color, setColor]}>{children}</Context.Provider>
  );
};

export default Context;
