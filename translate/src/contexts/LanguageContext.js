import React, { useState } from 'react';

const Context = React.createContext('en');

export const LanguageStore = ({ children }) => {
  const [language, setLanguage] = useState('en');

  return (
    <Context.Provider value={[language, setLanguage]}>
      {children}
    </Context.Provider>
  );
};

export default Context;
