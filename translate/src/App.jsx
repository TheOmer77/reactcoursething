import React from 'react';

// Components
import LanguageSelector from './components/LanguageSelector';
import UserCreate from './components/UserCreate';

// Contexts
import { LanguageStore } from './contexts/LanguageContext';
import { ColorStore } from './contexts/ColorContext';

const App = () => {
  return (
    <div className='ui basic segment'>
      <div className='ui container'>
        <LanguageStore>
          <LanguageSelector />
          <ColorStore>
            <UserCreate />
          </ColorStore>
        </LanguageStore>
      </div>
    </div>
  );
};

export default App;
