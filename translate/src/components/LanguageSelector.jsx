import React, { useContext } from 'react';
import LanguageContext from '../contexts/LanguageContext';

const LanguageSelector = () => {
  const [language, setLanguage] = useContext(LanguageContext);

  return (
    <div>
      Select a language:
      <i className='flag us' onClick={() => setLanguage('en')} />
      <i className='flag nl' onClick={() => setLanguage('nl')} />
    </div>
  );
};

export default LanguageSelector;
