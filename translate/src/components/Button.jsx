// Getting context value from Context.Provider

import React from 'react';
import LanguageContext from '../contexts/LanguageContext';
import ColorContext from '../contexts/ColorContext';

const Button = () => {
  return (
    <ColorContext.Consumer>
      {([color] /* Destructured! */) => (
        <button className={`ui ${color} button`}>
          <LanguageContext.Consumer>
            {([language] /* Destructured! */) =>
              language === 'en' ? 'Submit' : 'Voorleggen'
            }
          </LanguageContext.Consumer>
        </button>
      )}
    </ColorContext.Consumer>
  );
};

export default Button;
