// Getting context value with useContext

import React, { useContext } from 'react';
import LanguageContext from '../contexts/LanguageContext';
import ColorContext from '../contexts/ColorContext';

const Field = () => {
  const [language] = useContext(LanguageContext);
  const [color, setColor] = useContext(ColorContext);

  return (
    <div className='field'>
      <label>{language === 'en' ? 'Name' : 'Naam'}</label>
      <input value={color} onChange={(e) => setColor(e.currentTarget.value)} />
    </div>
  );
};

export default Field;
