import React, { Component } from 'react';
import ErrorDimmer from './components/ErrorDimmer';
import SeasonDisplay from './components/SeasonDisplay';
import Spinner from './components/Spinner';

class App extends Component {
  state = { lat: null, errMsg: '' };

  componentDidMount() {
    window.navigator.geolocation.getCurrentPosition(
      (position) => this.setState({ lat: position.coords.latitude }),
      (err) => this.setState({ errMsg: err.message })
    );
  }

  renderContent() {
    if (this.state.errMsg && !this.state.lat)
      return <ErrorDimmer message={`Error: ${this.state.errMsg}`} />;
    else if (!this.state.errMsg && this.state.lat)
      return <SeasonDisplay latitude={this.state.lat} />;
    else return <Spinner message='Please accept location access.' />;
  }

  render() {
    return this.renderContent();
  }
}

export default App;
