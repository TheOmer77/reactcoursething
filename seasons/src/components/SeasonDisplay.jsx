import React from 'react';

import './SeasonDisplay.css';

const [DEFAULT_SEASON, SUMMER, WINTER] = ['default', 'winter', 'summer'];
const seasonConfig = {
  default: {
    text: 'Loading...',
    icon: null,
  },
  winter: {
    text: "Burr, it's chilly!",
    icon: 'snowflake',
  },
  summer: {
    text: "Let's hit the beach!",
    icon: 'sun',
  },
};

const SeasonDisplay = ({ latitude }) => {
  const season = getSeason(latitude, new Date().getMonth());
  const { text, icon } = seasonConfig[season];
  return (
    <div className={`season-display ${season}`}>
      <i className={`icon-left massive ${icon} icon`}></i>

      <h1 className='season-text'>{text}</h1>
      <i className={`icon-right massive ${icon} icon`}></i>
    </div>
  );
};

const getSeason = (latitude, month) => {
  if (latitude) {
    if (month > 2 && month < 9) return latitude > 0 ? WINTER : SUMMER;
    else return latitude > 0 ? SUMMER : WINTER;
  } else return DEFAULT_SEASON;
};

export default SeasonDisplay;
