import React from 'react';

const ErrorDimmer = ({ message }) => {
  return (
    <div className='ui active dimmer'>
      <span className='ui inverted header'>{message}</span>
    </div>
  );
};

export default ErrorDimmer;
