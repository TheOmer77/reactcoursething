import React, { useState } from 'react';

// Components
import Header from './components/Header';
import Route from './components/Route';
import Accordion from './components/Accordion';
import Dropdown from './components/Dropdown';
import Search from './components/Search';
import Translate from './components/Translate';

const headerItems = [
  { name: 'Accordion', href: '/' },
  { name: 'Search', href: '/search' },
  { name: 'Dropdown', href: '/dropdown' },
  { name: 'Translate', href: '/translate' },
];

const accordionItems = [
  {
    title: 'What is react?',
    content: 'React is a JavaScript library for building user interfaces.',
  },
  {
    title: 'Why use react?',
    content: 'Because I said so, lol.',
  },
  {
    title: 'How do you use react?',
    content: "import React from 'react';",
  },
];

const dropdownOptions = [
  { label: 'Really Red', value: 'red' },
  { label: 'Great Green', value: 'green' },
  { label: 'Brilliant Blue', value: 'blue' },
];

const App = () => {
  const [selected, setSelected] = useState(dropdownOptions[0]);

  return (
    <div>
      <Header items={headerItems} />
      <div className='ui container'>
        <div className='ui basic segment'>
          <Route path='/'>
            <Accordion items={accordionItems} />
          </Route>
          <Route path='/search'>
            <Search />
          </Route>
          <Route path='/dropdown'>
            <div>
              <Dropdown
                title='Select a Color'
                options={dropdownOptions}
                selected={selected}
                onSelectedChange={setSelected}
              />
              <h4 className={`ui ${selected.value} header`}>
                This text is {selected.value}!
              </h4>
            </div>
          </Route>
          <Route path='/translate'>
            <Translate />
          </Route>
        </div>
      </div>
    </div>
  );
};

export default App;
