import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { sanitize } from 'dompurify';

const GOOGLE_TRANSLATE_API_URL =
  'https://translation.googleapis.com/language/translate/v2';
const GOOGLE_TRANSLATE_API_KEY = 'AIzaSyCHUCmpR7cT_yDFHC98CZJy2LTms-IwDlM';

const Convert = ({ language, text }) => {
  const [debouncedText, setDebouncedText] = useState('');
  const [translation, setTranslation] = useState(text);

  useEffect(() => {
    const timer = setTimeout(() => setDebouncedText(text), 500);
    return () => clearTimeout(timer);
  }, [text]);

  useEffect(() => {
    const doTranslation = async () => {
      const response = await axios.post(
        GOOGLE_TRANSLATE_API_URL,
        {},
        {
          params: {
            q: debouncedText,
            target: language.value,
            key: GOOGLE_TRANSLATE_API_KEY,
          },
        }
      );
      const { translations } = response.data.data;
      setTranslation(translations[0].translatedText);
    };
    doTranslation();
  }, [debouncedText, language]);

  return (
    <div>
      <h1
        className='ui header'
        dangerouslySetInnerHTML={{ __html: sanitize(translation) }}
      />
    </div>
  );
};

export default Convert;
