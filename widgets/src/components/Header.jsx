import React, { useEffect, useState } from 'react';

import Link from './Link';

const Header = ({ items }) => {
  const [currentPath, setCurrentPath] = useState(window.location.pathname);

  const onLocationChange = () => {
    setCurrentPath(window.location.pathname);
  };

  useEffect(() => {
    window.addEventListener('popstate', onLocationChange);
    return () => window.removeEventListener('popstate', onLocationChange);
  }, []);

  return (
    <div className='ui inverted basic segment'>
      <div className='ui container'>
        <div className='ui inverted secondary menu'>
          {items.map((item) => (
            <Link
              href={item.href}
              className={`${currentPath === item.href ? 'active ' : ''}item`}
              key={item.name}
            >
              {item.name}
            </Link>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Header;
