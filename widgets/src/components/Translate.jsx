import React, { useState } from 'react';

// Components
import Convert from './Convert';
import Dropdown from './Dropdown';

const options = [
  { label: 'English', value: 'en' },
  { label: 'German', value: 'de' },
  { label: 'French', value: 'fr' },
  { label: 'Spanish', value: 'es' },
  { label: 'Hebrew', value: 'he' },
];

const Translate = () => {
  const [language, setLanguage] = useState(options[0]);
  const [text, setText] = useState('');

  return (
    <div className='ui grid'>
      <div className='eight wide column'>
        <h4 className='ui header'>Input</h4>
        <Dropdown
          title='Language'
          options={options}
          selected={language}
          onSelectedChange={setLanguage}
        />
        <div className='ui form'>
          <div className='field'>
            <label>Text</label>
            <textarea value={text} onChange={(e) => setText(e.target.value)} />
          </div>
        </div>
      </div>
      <div className='eight wide column'>
        <h4 className='ui header'>Output</h4>
        <Convert language={language} text={text} />
      </div>
    </div>
  );
};

export default Translate;
