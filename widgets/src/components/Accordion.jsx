import React, { Fragment, useState } from 'react';

const Accordion = ({ items }) => {
  const [activeIndex, setActiveIndex] = useState(null);

  const onItemClick = (index) => {
    setActiveIndex(index);
  };

  return (
    <div className='ui styled accordion'>
      {items.map((item, index) => {
        const active = activeIndex === index ? 'active' : '';

        return (
          <Fragment key={item.title}>
            <div
              className={`title ${active}`}
              onClick={() => onItemClick(index)}
            >
              <i className='dropdown icon'></i>
              {item.title}
            </div>
            <div className={`content ${active}`}>
              <p>{item.content}</p>
            </div>
          </Fragment>
        );
      })}
    </div>
  );
};

export default Accordion;
