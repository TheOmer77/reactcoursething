import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { sanitize } from 'dompurify';

const Search = () => {
  const [term, setTerm] = useState('');
  const [debouncedTerm, setDebouncedTerm] = useState(term);
  const [results, setResults] = useState([]);

  useEffect(() => {
    const timer = setTimeout(() => {
      setDebouncedTerm(term);
    }, 1000);

    return () => {
      clearTimeout(timer);
    };
  }, [term]);

  useEffect(() => {
    const search = async () => {
      const { data } = await axios.get('https://en.wikipedia.org/w/api.php', {
        params: {
          action: 'query',
          list: 'search',
          origin: '*',
          format: 'json',
          srsearch: debouncedTerm,
        },
      });
      if (data.query) setResults(data.query.search);
      else setResults([]);
    };
    search();
  }, [debouncedTerm]);

  const renderedResults = results.map((result) => (
    <div className='item' key={result.pageid}>
      <div className='right floated content'>
        <a
          className='ui icon button'
          href={`https://en.wikipedia.org?curid=${result.pageid}`}
        >
          <i className='caret right icon' />
        </a>
      </div>
      <div className='content'>
        <div className='header'>{result.title}</div>
        <div
          className='description'
          dangerouslySetInnerHTML={{ __html: sanitize(result.snippet) }}
        />
      </div>
    </div>
  ));

  return (
    <div>
      <div className='ui form'>
        <div className='field'>
          <label>Enter Search Term</label>
          <input
            className='input'
            value={term}
            onChange={(e) => setTerm(e.target.value)}
          />
        </div>
      </div>
      <div className='ui relaxed divided list'>{renderedResults}</div>
    </div>
  );
};

export default Search;
