import React, { useState, useEffect, useRef } from 'react';

const Dropdown = ({ title, options, selected, onSelectedChange }) => {
  if (!options)
    throw new Error(
      'Options prop is missing. The options prop should be an array of objects with a label and value.'
    );

  const [isOpen, setOpen] = useState(false);
  const ref = useRef();

  useEffect(() => {
    const onBodyClick = (e) => {
      if (ref.current && !ref.current.contains(e.target)) setOpen(false);
    };

    document.body.addEventListener('click', onBodyClick);
    return () => document.body.removeEventListener('click', onBodyClick);
  }, []);

  const renderedOptions = options.map((option) => {
    if (option.value === selected.value) return null;
    return (
      <div
        key={option.value}
        className='item'
        onClick={() => onSelectedChange(option)}
      >
        {option.label}
      </div>
    );
  });

  return (
    <div className='ui form' ref={ref}>
      <div className='field'>
        <label className='label'>{title}</label>
        <div
          className={`ui selection dropdown ${isOpen && 'visible active'}`}
          onClick={() => setOpen(!isOpen)}
        >
          <i className='dropdown icon' />
          <div className='text'>
            {selected ? selected.label : 'Select one...'}
          </div>
          <div className={`menu ${isOpen && 'visible transition'}`}>
            {renderedOptions}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Dropdown;
