import React from 'react';

const Searchbar = ({ value, onChange, onSubmit }) => {
  return (
    <div className='ui segment'>
      <form className='ui form' onSubmit={onSubmit}>
        <div className='field'>
          <input
            type='text'
            name='search'
            id='searchInput'
            placeholder='Search images...'
            onChange={onChange}
            value={value}
          />
        </div>
      </form>
    </div>
  );
};

export default Searchbar;
