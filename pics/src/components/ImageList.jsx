import React from 'react';

import Image from './Image';

import './ImageList.css';

const ImageList = ({ images }) => {
  return (
    <div className='image-list'>
      {images.map((image) => (
        <Image image={image} key={image.id} />
      ))}
    </div>
  );
};

export default ImageList;
