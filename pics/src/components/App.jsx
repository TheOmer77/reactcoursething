import Searchbar from './Searchbar';
import React, { Component } from 'react';

import unsplash from '../api/unsplash';
import ImageList from './ImageList';

class App extends Component {
  state = { term: '', images: [] };

  handleSearchChange = (e) => {
    this.setState({ term: e.target.value });
  };

  handleSearchSubmit = async (e) => {
    e.preventDefault();
    const searchResult = await unsplash.get('/search/photos', {
      params: { query: this.state.term },
    });
    this.setState({ images: searchResult.data.results });
  };

  render() {
    return (
      <div className='ui container' style={{ padding: '1rem 0' }}>
        <Searchbar
          value={this.state.term}
          onChange={this.handleSearchChange}
          onSubmit={this.handleSearchSubmit}
        />
        <ImageList images={this.state.images} />
      </div>
    );
  }
}

export default App;
