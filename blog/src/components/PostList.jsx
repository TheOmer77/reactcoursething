import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchPostsAndUsers } from '../actions';
import UserHeader from './UserHeader';

class PostList extends Component {
  componentDidMount() {
    this.props.fetchPostsAndUsers();
  }

  renderList() {
    return this.props.posts.map((post) => (
      <div className='item' key={post.id}>
        <i className='large middle aligned user icon' />
        <div className='content'>
          <div className='description'>
            <h3 style={{ margin: 0 }}>{post.title}</h3>
            <p>{post.body}</p>
          </div>
        </div>
        <UserHeader userId={post.userId} />
      </div>
    ));
  }

  render() {
    return <div className='ui relaxed divided list'>{this.renderList()}</div>;
  }
}

const mapStateToProps = (state) => ({
  posts: state.posts,
});

export default connect(mapStateToProps, { fetchPostsAndUsers })(PostList);
