import React from 'react';
import unescapeHTML from '../utils/unescapeHTML';

const VideoView = ({ video }) => {
  const { title, channelTitle, publishedAt, description } = video.snippet;
  const videoId = video.id;
  const videoSrc = `https://www.youtube.com/embed/${videoId}?autoplay=1`;

  return (
    <div>
      <div className='ui embed'>
        <iframe
          src={videoSrc}
          title='video player'
          allow='autoplay'
          allowFullScreen
        />
      </div>
      <div className='ui basic segment'>
        <h2 className='ui header'>
          {unescapeHTML(title)}
          <div className='sub header'>{`${unescapeHTML(
            channelTitle
          )}, ${new Date(publishedAt).toLocaleDateString()}`}</div>
        </h2>
        <div style={{ whiteSpace: 'pre-wrap' }}>{description}</div>
      </div>
    </div>
  );
};

export default VideoView;
