import React, { Component } from 'react';

// Components
import Header from './Header';
import VideoList from './VideoList';
import VideoView from './VideoView';

import youtube from '../api/youtube';

class App extends Component {
  state = { videos: [], currentVideo: null, error: null };

  componentDidMount() {
    this.handleSearchSubmit('');
  }

  handleSearchSubmit = async (term) => {
    try {
      const response = await youtube.get('/search', {
        params: { q: term },
      });
      this.setState({ videos: response.data.items, currentVideo: null });
    } catch (err) {
      console.log(err);
      this.setState({ error: err.message });
    }
  };

  handleVideoSelect = async (video) => {
    const { videoId } = video.id;
    const response = await youtube.get('/videos', {
      params: { id: videoId },
    });
    this.setState({ currentVideo: response.data.items[0] });
  };

  render() {
    return (
      <div>
        <Header onSearchSubmit={this.handleSearchSubmit} />
        <div className='ui container'>
          <div className='ui grid'>
            <div className='ui row'>
              {this.state.currentVideo && (
                <div className='eleven wide column'>
                  <VideoView video={this.state.currentVideo} />
                </div>
              )}
              <div
                className={`${
                  this.state.currentVideo ? 'five' : 'sixteen'
                } wide column`}
              >
                {this.state.videos.length > 0 ? (
                  <VideoList
                    videos={this.state.videos}
                    onVideoSelect={this.handleVideoSelect}
                  />
                ) : this.state.error ? (
                  <span>Could not fetch videos: {this.state.error}</span>
                ) : (
                  <span>Loading...</span>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
