import React from 'react';

const Searchbar = ({ value, onChange }) => {
  return (
    <div className='ui inverted left icon input searchbar'>
      <input
        type='text'
        placeholder='Search videos...'
        value={value}
        onChange={onChange}
      />
      <i className='search icon'></i>
    </div>
  );
};

export default Searchbar;
