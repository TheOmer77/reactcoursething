import React from 'react';
import VideoListItem from './VideoListItem';

const VideoList = ({ videos, onVideoSelect }) => {
  return (
    <div className='ui fluid secondary vertical menu'>
      {videos.map((video) => (
        <VideoListItem
          video={video}
          key={video.id.videoId}
          onClick={onVideoSelect}
        />
      ))}
    </div>
  );
};

export default VideoList;
