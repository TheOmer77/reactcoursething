import React, { Component } from 'react';
import Searchbar from './Searchbar';

import './Header.css';

class Header extends Component {
  state = { term: '' };

  handleFormSubmit = (e) => {
    e.preventDefault();
    this.props.onSearchSubmit(this.state.term);
  };

  render() {
    return (
      <div className='ui red inverted basic segment'>
        <div className='ui container _header'>
          <form className='ui form' onSubmit={this.handleFormSubmit}>
            <div className='two fields'>
              <div className='field logo'>
                <i className='big video icon'></i>
                <h1>FakeTube</h1>
              </div>
              <div className='field'>
                <Searchbar
                  value={this.state.term}
                  onChange={(e) => this.setState({ term: e.target.value })}
                />
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default Header;
