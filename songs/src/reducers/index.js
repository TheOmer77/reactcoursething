import { combineReducers } from 'redux';

// Reducers
const songsReducer = () => {
  return [
    { title: 'Random', duration: '2:34' },
    { title: 'Jump', duration: '3:32' },
    { title: 'I ran out of song names', duration: '4:05' },
    { title: "One second song (yes it's just one second)", duration: '0:01' },
  ];
};

const selectedSongReducer = (selectedSong = null, action) => {
  if (action.type === 'SONG_SELECTED') return action.payload;
  return selectedSong;
};

export default combineReducers({
  songs: songsReducer,
  selectedSong: selectedSongReducer,
});
