import React from 'react';
import { connect } from 'react-redux';

const SongDetails = ({ song }) => {
  return (
    <>
      <div style={{ margin: 0 }} className='ui header'>
        {song ? 'Details for' : 'Please select a song.'}
      </div>
      {song && (
        <div>
          <h1 style={{ margin: 0 }}>{song.title}</h1>
          <div>Duration: {song.duration}</div>
        </div>
      )}
    </>
  );
};

const mapStateToProps = (state) => {
  return { song: state.selectedSong };
};

export default connect(mapStateToProps)(SongDetails);
