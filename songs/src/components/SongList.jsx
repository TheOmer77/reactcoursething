import React from 'react';
import { connect } from 'react-redux';

import { selectSong } from '../actions';

const SongList = (props) => {
  const renderedList = props.songs.map((song) => (
    <div className='item' key={song.title}>
      <div className='right floated content'>
        <button
          className='ui button primary'
          onClick={() => props.selectSong(song)}
        >
          Select
        </button>
      </div>
      <div className='content'>
        <div className='header'>{song.title}</div>
        <div className='description'>{song.duration}</div>
      </div>
    </div>
  ));

  return <div className='ui relaxed divided list'>{renderedList}</div>;
};

const mapStateToProps = (state) => {
  return { songs: state.songs };
};

export default connect(mapStateToProps, { selectSong })(SongList);
