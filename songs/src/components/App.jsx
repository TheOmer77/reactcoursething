import React from 'react';

import SongList from './SongList';
import SongDetails from './SongDetails';

const App = () => {
  return (
    <div className='ui container'>
      <div className='ui basic segment'>
        <div className='ui stackable grid'>
          <div className='eight wide column'>
            <SongDetails />
          </div>
          <div className='eight wide column'>
            <SongList />
          </div>
        </div>
      </div>
    </div>
  );
};

export default App;
